// hello.c
#include <stdio.h> // don't forget this.

int main()
{
	// this is very important.
	printf("Hello, world!\n");
	printf("Hello, again.\n");
	
	
	// it won't change output, though.
	return 0;
}